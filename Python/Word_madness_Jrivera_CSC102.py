# Juan Rivera
# 7/3/17
# Word Madness CSC102


consonants = "bcdfghjklmnpqrstvexz" # Identifying and setting the value to both consonants and vowels
vowels = "aeiouy"
correct = False #Main loop
while not correct:
    choice = input("Please enter the one you want to count consonant or vowels, if you would like to quit type stop. ") #user input limited to ONLY consonants, vowels and the quit option
    if choice =="consonant": #String for consonant
        print ("Mode Consonant") #letting the user know its consonant mode
        correct = True
        ccount = False
        while not ccount:  # loop for counting consonants in word(s)
            words=input("Please enter a word ") #input for entering a word
            print ("Amount of consonants is", sum(words.count(c) for c in consonants)) # line for counting consonants in words
            ask = input ("Do you wish to enter another word? Enter yes or no ") #line asking the user if they want to do another words untill they say no
            if ask == "no":
                ccount=True
                exit
    elif choice == "vowels": #Vowel side of the loop
        print ("Mode Vowel")# letting the user know its the vowel mode 
        correct = True
        vcount=False #Loop for vounting vowels
        while not vcount:
          words=input("Please enter a word ")
          print ("Amount of vowels is", sum(words.count(c) for c in vowels)) # Set to count the vowel list defined up above
          ask = input ("Do you wish to enter another word? Enter yes or no ")
          if ask == "no":
            vcount=True         #loop break if they wish to stop
            exit
        
    elif choice == "stop":  #exit choice for first input
        correct = True
        exit
    elif not "consonant" or "vowels" or "stop": #limits first choice to the 3 options
        print("Please choose one of the options")

    
    
   
