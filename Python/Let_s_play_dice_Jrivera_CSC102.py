# Juan Rivera
# 6/23/16
# CSC 102
# Let's play dice assignment 

from random import randint

Randomnumber1=randint(1,6)
Randomnumber2=randint(1,6)  #Setting up the random number command

print ("Dice number 1 rolls", Randomnumber1)    #Randomnumber1
print ("Dice number 2 rolls", Randomnumber2)    #Randomnumber2

print ("The sum of Dice one and two is", Randomnumber1+Randomnumber2) # Addition line
if (Randomnumber1 < Randomnumber2):
    print ("The difference of Dice one and two is", Randomnumber2-Randomnumber1)# These 3 commands make sure that it is subtracted properly 
elif (Randomnumber1 > Randomnumber2):
    print ("The difference of Dice one and two is", Randomnumber1-Randomnumber2)
elif (Randomnumber1 == Randomnumber2):
    print ("The difference of Dice one and two is", Randomnumber1-Randomnumber2)

print ("The product of dice one and two is", Randomnumber1*Randomnumber2) # Multiplication line

print ("The quotient of dice one and two is", Randomnumber1/Randomnumber2) #Division line

sum= Randomnumber1+Randomnumber2 # Setting up the average equation
print ("The average of the two dices is", sum/2) # Writes out and finishes the average equation


