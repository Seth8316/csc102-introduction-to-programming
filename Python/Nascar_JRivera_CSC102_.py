#Juan Rivera
# 7/9/17
#CSC102 NASCAR

from random import randint
class Car: #class cars
    def __init__  (self, odo_miles, speed, driver, sponsor): # defines the basis for the class car each of these selfs is for the class car
        self.odo_miles = odo_miles
        self.speed = speed
        self.driver = driver
        self.sponsor = sponsor

    def updateMinute(self): # updates the minute for the race, sets the miles to speed, sets the ramdom increments for the distance, esstenially the main part of the program
        self.odo_miles += self.speed

        if self.odo_miles >= 500:
            return True
        self.speed = randint(1,120)
        return False

cars = []   #The drivers and their sponsors
cars.append(Car(0, 0, 'Bob Ross', 'Art cars'))
cars.append(Car(0, 0, 'Mario', 'Nintendo'))
cars.append(Car(0, 0, 'Rick Ross', 'Pawnshop'))
cars.append(Car(0, 0, 'Link', 'Hyrule'))
cars.append(Car(0, 0, 'Anakin', 'Tatooine'))
cars.append(Car(0, 0, 'Smeagle', 'One ring'))
cars.append(Car(0, 0, 'Batman', 'Gotham'))
cars.append(Car(0, 0, 'Rick Astley', 'Never gonna give up'))
cars.append(Car(0, 0, 'Aragon', 'Gondor'))
cars.append(Car(0, 0, 'Gandalf', 'You shall not pass'))
cars.append(Car(0, 0, 'Pikachu', 'Ash'))
cars.append(Car(0, 0, 'Michealangelo', 'splinter'))
cars.append(Car(0, 0, 'Shreder', 'Foot Ninjas'))
cars.append(Car(0, 0, 'Morty', 'Rick'))
cars.append(Car(0, 0, 'DoctorWho', 'David Tennant'))
cars.append(Car(0, 0, 'Harry Potter', 'Gryffindor'))
cars.append(Car(0, 0, 'TonyStark', 'StarkIndustries'))
cars.append(Car(0, 0, 'Tom', 'Jerry'))
cars.append(Car(0, 0, 'Sparticus', 'Lazytown'))
cars.append(Car(0, 0, 'Zeus', 'Olympus'))

raceend=False #race is still going
while raceend == False: #loop until one driver reaches 500
    for c in cars:
        if raceend == True: 
            break
        finished = c.updateMinute()
        if finished == True:
            raceend = True
            print("The one to cross the finish line first is " + c.driver + ", " + c.sponsor + "!") #winner message
