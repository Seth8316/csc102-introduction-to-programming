#Juan Rivera
#7/9/17
#CSC102 Stock portfolio part 2


stockc = 0 #stock count
def addName():          #Function for stating the dictionary variables
    global stocksym #States variables are global
    global FName
    global stocksym2
    if stockc == 0: #this is for stock key one
        stocksym=input("Please enter the first stock symbol. ")
    elif stockc==1: #this is for stock key 2
        stocksym2=input("Please enter the second stock symbol. ")
    FName=input("Please enter the full name. ") #Enters full name
def addPrices():    #Functions for adding prices
    global buy # states variables are global for prices
    global market
    if stockc == 0: #For stock key one
        buy=input ("What was the buy price of " + stocksym +"? ")
        market =input("What is the market value of " + stocksym +"? ")
    elif stockc == 1: # for stock key two
        buy=input ("What was the buy price of " + stocksym2 +"? ")
        market =input("What is the market value of " + stocksym2 +"? ")

def addExposure(): # Function for Exposure
    global shares # states global variables for Exposure
    global risk
    if stockc == 0: # for stock key one
        shares = input ("Please enter the amount of " + stocksym + " shares bought. ")
        risk =  input ("Please enter the risk percentage of " + stocksym + " in decimal format. " )
    elif stockc == 1: #for stock key two
        shares = input ("Please enter the amount of " + stocksym2 + " shares bought. ")
        risk =  input ("Please enter the risk percentage of " + stocksym2 + " in decimal format. " )
    
    
  
def addStock(): #function for adding stocks
   
    global Names #allows all 3 directories to be global
    global Prices
    global Exposure
    addName()
    addPrices()
    addExposure()
    Names = {stocksym: FName}
    Prices= {stocksym: [float(buy), float(market)]}
    Exposure={stocksym: [float (shares), float (risk)]}
    main()

   
def Getsale(): #getsale function
    sale = ((float (market) - float (buy)) - float(risk)) * float(market) * float(shares)
    print ('The recommend sale of', stocksym, 'is', sale )
    main()
    
        

    
   
        
def main(): # Main function
    print("You can chose one of three options 1.add stock, 2.Recommend sale or 3. Exit ") #option list

 
    option = input("Please select 1, 2 or 3 ") #user input for options
    

    print("You have chosen number", option) # lets them know what option they chose

    if option == '1': #option 1 for add stock
        addStock()
    elif option == '2' : #option 2 to get sale
        Getsale()
    elif option == '3' : #option 3 to exit 
        exit
 
     
main() #runs everything
    
