# Juan Rivera
#7/8/17
#Lab4 guessing game part 3 CSC102 


import random #Random import
number = random.randint(1,10) #Number generator
correct = False
guesses = 0

def signs():
    if (int(guess > number)):
        print(int(guess), "> number")
    elif (guess < number):
        print(int(guess), "< number")
    return 
    
while not correct:                    #Begining of Loop
    print ("I'm thinking of a number between 1 and 10")
    guess = int(input("Guess! "))  #First guess
    if (int(guess > number)):            # Response when number is too high
        guesses += 1
        signs()
        print("Number is too high ")
        Continue = int(input ("Do You wish to continue? Enter 1 for yes and 0 for no ")) #Allows users choice of continueing or not
        if (Continue == 1):   
            continue
        elif (Continue == 0):
            break
    elif (guess < number):  #Response if number is too low
        guesses += 1
        signs()
        print ("Number is too low")
        Continue = int(input ("Do You wish to continue? Enter 1 for yes and 0 for no "))
        if (Continue == 1):
            continue
        elif (Continue == 0):
            break #Breaks the Continue loop
    if guess == number:           #Action taken when number is correctly guessed
         print ("Correct!")
         correct = True # Breaks the loop by setting correct value to true
print (guesses)

with open('Number of guesses.txt','w') as n:
    n.write ('%d' % guesses)
