# Juan Rivera
# 7/15/17
# Final project: Zoo part 2 CSC 102
class Animals:      # Parent Class
    def __init__(self, Type, age, color, gender, breed):  # main __init__ function, all subclasss inherit it
        self.animalType = Type
        self.age = age 
        self.color = color
        self.gender = gender
        self.breed = breed
    def makeNoise(): #Make noise function
        self.makeNoise = noise
    
class dog(Animals): #First Subclass 
    noise = 'woof! woof! woof!' #Changes each one the the noise they make
    def __init__(self, Type, age, color, gender, breed): #This command recalls the parent __init__ function, this command is on all subclasses
            super(dog, self).__init__(Type, age, color, gender, breed)# This command also points the __init__ function to the parent one
class dog2(Animals): #Second Subclass
    noise = 'woof! woof! woof!'
    def __init__(self, Type, age, color, gender, breed):
        super(dog2, self).__init__(Type, age, color, gender, breed)
class Cat(Animals): # Third Subclass 
    noise = 'Meow! Meow! Meow!'
    def __init__(self, Type, age, color, gender, breed):
        super(Cat, self).__init__(Type, age, color, gender, breed)
class Cat2(Animals): # Fourth Subclass
    noise = 'Meow! Meow! Meow!'
    def __init__(self, Type, age, color, gender, breed):
        super(Cat2, self).__init__(Type, age, color, gender, breed)
class Lion(Animals): #Fifth Subclass
    noise = 'Roaaaaaar!'
    def __init__(self, Type, age, color, gender, breed):
        super(Lion, self).__init__(Type, age, color, gender, breed)
class Lion2(Animals): #Sixth Subclass
    noise = 'Roaaaaaar!'
    def __init__(self, Type, age, color, gender, breed):
        super(Lion2, self).__init__(Type, age, color, gender, breed)
        
parent= Animals('Type', 'age', 'color', 'gender', 'breed')
A = dog('Dog', 4, 'White and brown spotted', 'Male', 'German Shepard') # Attributes for each subclass and their other instance, I hope this does not count as polymorphism
B = dog2('Dog', 3, 'Black and white', 'Female', 'Yorkie Terrier')
C = Cat('Cat', 5, 'Black and white spotted', 'Female', 'American Wirehair cat')
D = Cat2('Cat', 2, 'Black', 'Male', 'Bombay cat')
E = Lion('Lion', 3, 'Orange', 'Female', 'West African Lion')
F = Lion2('Lion', 6, 'Orange', 'Male', 'West African Lion')

Zoolist = [A.__dict__, B.__dict__, C.__dict__, D.__dict__, E.__dict__,F.__dict__]#Subclasses in the list Zoolist

print ('This is an example of how the information will be presented.')
print (parent.__dict__) # Prints original attribuites but explains how the rest are presented 
print ('Animal noise')
zoolist=False
while not zoolist:      # Loop to traverse through the Zoolist

    print (Zoolist[0])  #print for each zoolist
    print ('This is the sound dogs make', A.noise)

    print (Zoolist[1])
    print ('This is the sound dogs make', B.noise)

    print (Zoolist[2])
    print ('This is the sound Cats make', C.noise)

    print (Zoolist[3])
    print ('This is the sound Cats make', D.noise)

    print (Zoolist[4])
    print ('This is the sound Lion make', E.noise)

    print (Zoolist[5])
    print ('This is the sound Lion make', F.noise)
    zoolist= True
    





