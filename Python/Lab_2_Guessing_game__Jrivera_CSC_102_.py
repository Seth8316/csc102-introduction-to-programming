#Juan Rivera
#6/21/17
#Lab 2 guessing game 
#######################
import random
number = random.randint(1,10) #Number generator

print ("I'm thinking of a number between 1 and 10") # Introduction
while True:                    #Begining of Loop
    guess = int(input("Guess! "))  #First guess
    if (guess > number):            # Response when number is too high
        print("Number is too high ")
    elif (guess < number):           #Response if number is too low
        print ("Number is too low")
    elif guess == number:           #Action taken when number is correctly guessed
        break
print ("Correct!")
